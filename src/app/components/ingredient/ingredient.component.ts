import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Recipes } from 'src/app/Module/Recipes';
import { BackendapiService } from 'src/app/services/backendapi.service';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent implements OnInit {

  ingredients: Recipes[] = [];

  form: FormGroup;

  constructor(private ingredientSrv: BackendapiService) { }

  ngOnInit(): void {
    this.getAllIngredients();
  }

  getAllIngredients() {
    this.ingredientSrv.getAllFoods()
    .toPromise()
    .then((ingredients) => {
      console.log(ingredients);
      this.ingredients = ingredients;
    });
  }

}
