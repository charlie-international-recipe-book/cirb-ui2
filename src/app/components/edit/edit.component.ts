import { HttpHeaders, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NumberValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  responseC: any;
  responseF: any;
  responseI: any;

  idC: number;
  idF: number;
  idI: number;

  submittedC: boolean;
  submittedF: boolean;
  submittedI: boolean;

  cName: string;
  cLike: string;
  cLoc: string;

  fName: string;
  fPrice: string;
  fLoc: string;
  fIN: string;

  iName: string;
  iPrice: string;
  iLoc: string;
  iDeet: string;

  fgCountry = new FormGroup({
    name: new FormControl(''),
    cl: new FormControl(''),
    loc: new FormControl('')
  });

  fgFood = new FormGroup({
    price: new FormControl(''),
    loc: new FormControl(''),
    name: new FormControl(''),
    iname: new FormControl('')
  })

  fgIng = new FormGroup({
    ingdet: new FormControl(''),
    price: new FormControl(''),
    name: new FormControl(''),
    loc: new FormControl('')

  })

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  editCountry(id: number, name: string, cl: string, loc: string){

    while (name.indexOf(' ') > 0){
      name = name.replace(' ', '%20');
    }
    while (cl.indexOf(' ') > 0){
      cl = cl.replace(' ', '%20');
    }
    while (loc.indexOf(' ') > 0){
      loc = loc.replace(' ', '%20');
    }
    this.http.post(`http://localhost:8080/recipiebook/edit/country/${id}?country_name=${name}&country_likeness=${cl}&location=${loc}`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'})
    .subscribe(x => {this.responseC = x;
      console.log(x);
    },
    (bad: HttpErrorResponse) => {
      console.log("error: " + bad.error);
      console.log("name: " + bad.name);
      console.log("message: " + bad.message);
      console.log("status: " + bad.status);
    });

  }

  editFood(id: number, name: string, price: string, loc: string, iname: string){

    while (name.indexOf(' ') > 0){
      name = name.replace(' ', '%20');
    }
    while (price.indexOf(' ') > 0){
      price = price.replace(' ', '%20');
    }
    while (loc.indexOf(' ') > 0){
      loc = loc.replace(' ', '%20');
    }
    while (iname.indexOf(' ') > 0){
      iname = iname.replace(' ', '%20');
    }

    this.http.post(`http://localhost:8080/recipiebook/edit/food/${id}?food_name=${name}&food_price=${price}&location=${loc}&ingredient_name=${iname}`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'})
    .subscribe(x => {this.responseC = x; console.log(x)},
    (bad: HttpErrorResponse) => {
      console.log("error: " + bad.error);
      console.log("name: " + bad.name);
      console.log("message: " + bad.message);
      console.log("status: " + bad.status);
    });
  }

  editIngredient(id: number, name: string, price: string, loc: string, deet: string){

    while (name.indexOf(' ') > 0){
      name = name.replace(' ', '%20');
    }
    while (price.indexOf(' ') > 0){
      price = price.replace(' ', '%20');
    }
    while (loc.indexOf(' ') > 0){
      loc = loc.replace(' ', '%20');
    }
    while (deet.indexOf(' ') > 0){
      deet = deet.replace(' ', '%20');
    }
    this.http.post(`http://localhost:8080/recipiebook/edit/ingredients/${id}?ingredient_name=${name}&ingredient+price=${price}&location=${loc}&ingredient_details=${deet}`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'})
    .subscribe(x => {this.responseC = x; console.log(x)},
    (bad: HttpErrorResponse) => {
      console.log("error: " + bad.error);
      console.log("name: " + bad.name);
      console.log("message: " + bad.message);
      console.log("status: " + bad.status);
    });
  }

  submitCountry(){
    this.editCountry(this.idC,
      this.fgCountry.get('name').value,
      this.fgCountry.get('cl').value,
      this.fgCountry.get('loc').value);
    this.fgCountry.disable();
    this.submittedC = true;
  }
  submitFood(){
    this.editFood(this.idF,
      this.fgFood.get('name').value,
      this.fgFood.get('price').value,
      this.fgFood.get('loc').value,
      this.fgFood.get('iname').value);
    this.fgFood.disable();
    this.submittedF = true;
  }
  submitIng(){
    this.editIngredient(this.idI,
      this.fgIng.get('name').value,
      this.fgIng.get('price').value,
      this.fgIng.get('loc').value,
      this.fgIng.get('ingdet').value);
    this.fgIng.disable();
    this.submittedI = true;
  }

  checkCountry(id: number){
    this.http.get(`http://localhost:8080/recipiebook/edit/country/${this.idC}/n`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {
      this.cName = name;
      console.log(this.cName);
      this.fgCountry.patchValue({name: this.cName});
    });
    
    this.http.get(`http://localhost:8080/recipiebook/edit/country/${this.idC}/x`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {
      this.cLike = name;
      console.log(this.cLike);
      this.fgCountry.patchValue({cl: this.cLike});
    });

    this.http.get(`http://localhost:8080/recipiebook/edit/country/${this.idC}/l`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.cLoc = name;
      console.log(this.cLoc)
      this.fgCountry.patchValue({loc: this.cLoc});
      });
  }

  checkFood(id: number){
    this.http.get(`http://localhost:8080/recipiebook/edit/food/${this.idF}/n`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.fName = name;
      console.log(this.fName)
      this.fgFood.patchValue({name: this.fName});
    });
    
    this.http.get(`http://localhost:8080/recipiebook/edit/food/${this.idF}/p`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.fPrice = name;
      console.log(this.fPrice)
      this.fgFood.patchValue({price: this.fPrice});
    });

    this.http.get(`http://localhost:8080/recipiebook/edit/food/${this.idF}/l`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.fLoc = name;
      console.log(this.fLoc)
      this.fgFood.patchValue({loc: this.fLoc});
    });

    this.http.get(`http://localhost:8080/recipiebook/edit/food/${this.idF}/i`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.fIN = name;
      console.log(this.fIN)
      this.fgFood.patchValue({iname: this.fIN});
    });

  }

  checkIng(id: number){
    this.http.get(`http://localhost:8080/recipiebook/edit/ingredients/${this.idI}/n`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.iName = name;
      console.log(this.iName)
      this.fgIng.patchValue({name: this.iName});
    });
    
    this.http.get(`http://localhost:8080/recipiebook/edit/ingredients/${this.idI}/p`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.iPrice = name;
      console.log(this.iPrice)
      this.fgIng.patchValue({price: this.iPrice});
    });

    this.http.get(`http://localhost:8080/recipiebook/edit/ingredients/${this.idI}/l`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.iLoc = name;
      console.log(this.iLoc)
      this.fgIng.patchValue({loc: this.iLoc});
    });

    this.http.get(`http://localhost:8080/recipiebook/edit/ingredients/${this.idI}/d`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.iDeet = name;
      console.log(this.iDeet)
      this.fgIng.patchValue({ingdet: this.iDeet});
    });

  }
}
