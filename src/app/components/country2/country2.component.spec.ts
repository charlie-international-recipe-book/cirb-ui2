import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Country2Component } from './country2.component';

describe('Country2Component', () => {
  let component: Country2Component;
  let fixture: ComponentFixture<Country2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Country2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Country2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
