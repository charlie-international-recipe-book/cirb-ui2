import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';
import { BackendapiService } from 'src/app/services/backendapi.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  responseC: any;
  responseF: any;
  responseI: any;
  booleanC: boolean;
  booleanI: boolean;
  booleanF: boolean;
  idC: number;
  idF: number;
  idI: number;
  cName: string;
  cLike: string;
  cLoc: string;
  fName: string;
  fPrice: string;
  fLoc: string;
  fIN: string;
  iName: string;
  iPrice: string;
  iLoc: string;
  iDeet: string;
  observing: any = null;
  fgCountry = new FormGroup({
    name: new FormControl(''),
    cl: new FormControl(''),
    loc: new FormControl('')
  });
  fgFood = new FormGroup({
    price: new FormControl(''),
    loc: new FormControl(''),
    name: new FormControl(''),
    iname: new FormControl('')
  })
  fgIng = new FormGroup({
    ingdet: new FormControl(''),
    price: new FormControl(''),
    name: new FormControl(''),
    loc: new FormControl('')
  })
  constructor(private srv: BackendapiService, private router: Router) { }
  ngOnInit(): void {
    // this.srv.startList().subscribe(stuff => this.observing = stuff);
    // this.srv.getList();
  }
  submit(){
    let formy = new FormData();
    formy.append('name', this.fgCountry.get('name').value);
    formy.append('cl', this.fgCountry.get('cl').value);
    formy.append('loc', this.fgCountry.get('loc').value);
    formy.append('price', this.fgFood.get('price').value);
    formy.append('loc', this.fgFood.get('loc').value);
    formy.append('name', this.fgFood.get('name').value);
    formy.append('iname', this.fgFood.get('iname').value);
    formy.append('ingdet', this.fgIng.get('ingdet').value);
    formy.append('price', this.fgIng.get('price').value);
    formy.append('name', this.fgIng.get('name').value);
    formy.append('loc', this.fgIng.get('iname').value);
    // this.srv.addCountry(
    // this.fgCountry.get('name').value,
    // this.fgCountry.get('cl').value,
    // this.fgCountry.get('loc').value,
    // formy);
    // this.srv.addFood(
    // this.fgFood.get('name').value,
    // this.fgFood.get('price').value,
    // this.fgFood.get('location').value,
    // this.fgFood.get('iname').value,
    // formy);
    // this.srv.addIng(
    // this.fgIng.get('name').value,
    // this.fgIng.get('price').value,
    // this.fgIng.get('location').value,
    // this.fgIng.get('ingdet').value,
    // formy);
    console.log(this.fgCountry.get('name').value);
    console.log(this.fgCountry.get('cl').value);
    console.log(this.fgCountry.get('loc').value);
    console.log(this.fgFood.get('name').value);
    console.log(this.fgFood.get('price').value);
    console.log(this.fgFood.get('location').value);
    console.log(this.fgFood.get('iname').value);
    console.log(this.fgIng.get('name').value);
    console.log(this.fgIng.get('price').value);
    console.log(this.fgIng.get('location').value);
    console.log(this.fgIng.get('iname').value);
    this.fgCountry.reset();
    this.fgFood.reset();
    this.fgIng.reset();
    this.router.navigate(['']);
  }
}