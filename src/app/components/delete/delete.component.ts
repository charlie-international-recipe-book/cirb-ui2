import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  responseC: any;
  responseF: any;
  responseI: any;

  booleanC: boolean;
  booleanI: boolean;
  booleanF: boolean;

  idC: number;
  idF: number;
  idI: number;
  cName: string;
  cLike: string;
  cLoc: string;
  fName: string;
  fPrice: string;
  fLoc: string;
  fIN: string;
  iName: string;
  fgIng: any;
  iPrice: string;
  iLoc: string;
  iDeet: string;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  deleteCountry(id: number){
    this.responseC = this.http.delete(`http://localhost:8080/recipiebook/delete/country/${id}`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).subscribe(x => {this.responseC = x; console.log(x)},
    (bad: HttpErrorResponse) => {
      console.log("error: " + bad.error);
      console.log("name: " + bad.name);
      console.log("message: " + bad.message);
      console.log("status: " + bad.status);
    });
  }

  deleteFood(id: number){
    this.responseF = this.http.delete(`http://localhost:8080/recipiebook/delete/food/${id}`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).subscribe(x => {this.responseF = x; console.log(x)},
    (bad: HttpErrorResponse) => {
      console.log("error: " + bad.error);
      console.log("name: " + bad.name);
      console.log("message: " + bad.message);
      console.log("status: " + bad.status);
    });
  }

  deleteIngredient(id: number){
    this.responseI = this.http.delete(`http://localhost:8080/recipiebook/delete/ingredient/${id}`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).subscribe(x => {this.responseI = x; console.log(x)},
    (bad: HttpErrorResponse) => {
      console.log("error: " + bad.error);
      console.log("name: " + bad.name);
      console.log("message: " + bad.message);
      console.log("status: " + bad.status);
    });
  }


  checkCountry(id: number){
    this.http.get(`http://localhost:8080/recipiebook/edit/country/${this.idC}/n`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {
      this.cName = name;
      console.log(this.cName);
    });
    
    this.http.get(`http://localhost:8080/recipiebook/edit/country/${this.idC}/x`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {
      this.cLike = name;
      console.log(this.cLike);
    });

    this.http.get(`http://localhost:8080/recipiebook/edit/country/${this.idC}/l`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.cLoc = name;
      console.log(this.cLoc)
      });

      this.booleanC = true;
  }

  checkFood(id: number){
    this.http.get(`http://localhost:8080/recipiebook/edit/food/${this.idF}/n`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.fName = name;
      console.log(this.fName)
    });
    
    this.http.get(`http://localhost:8080/recipiebook/edit/food/${this.idF}/p`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.fPrice = name;
      console.log(this.fPrice)
    });

    this.http.get(`http://localhost:8080/recipiebook/edit/food/${this.idF}/l`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.fLoc = name;
      console.log(this.fLoc)
    });

    this.http.get(`http://localhost:8080/recipiebook/edit/food/${this.idF}/i`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.fIN = name;
      console.log(this.fIN)
    });

    this.booleanF = true;
  }

  checkIng(id: number){
    this.http.get(`http://localhost:8080/recipiebook/edit/ingredients/${this.idI}/n`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.iName = name;
      console.log(this.iName)
      this.fgIng.patchValue({name: this.iName});
    });
    
    this.http.get(`http://localhost:8080/recipiebook/edit/ingredients/${this.idI}/p`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.iPrice = name;
      console.log(this.iPrice)
    });

    this.http.get(`http://localhost:8080/recipiebook/edit/ingredients/${this.idI}/l`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.iLoc = name;
      console.log(this.iLoc)
    });

    this.http.get(`http://localhost:8080/recipiebook/edit/ingredients/${this.idI}/d`,
    {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text'}).toPromise()
    .then(name => {this.iDeet = name;
      console.log(this.iDeet)
    });

    this.booleanI = true;
  }

  countryChanged(){
    this.booleanC = false;
    this.responseC = "";
  }
  foodChanged(){
    this.booleanF = false;
    this.responseF = "";
  }
  ingChanced(){
    this.booleanI = false;
    this.responseI = "";
  }
}
