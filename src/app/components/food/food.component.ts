import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Recipes } from 'src/app/Module/Recipes';
import { BackendapiService } from 'src/app/services/backendapi.service';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {

  foods: Recipes[] = [];

  form: FormGroup;

  constructor(private foodSrv: BackendapiService) { }

  ngOnInit(): void {
    this.getAllFoods();
  }

  getAllFoods() {
    this.foodSrv.getAllFoods()
    .toPromise()
    .then((foods) => {
      console.log(foods);
      this.foods = foods;
    });
  }

}
