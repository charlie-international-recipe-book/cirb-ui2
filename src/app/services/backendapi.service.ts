import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Recipes } from '../Module/Recipes';

@Injectable({
  providedIn: 'root'
})
export class BackendapiService {

  anyVar: any = null;
  observedItem = new BehaviorSubject<any>(this.anyVar);


  constructor(private http: HttpClient) { }

  startingList(): Observable<any> {
    return this.observedItem.asObservable();
  }

  getAllFoods() {
    return this.http.get(`${environment.url}/foods`) as Observable<Recipes[]>;
  }

  getAllIngredients() {
    return this.http.get(`${environment.url}/ingredients`) as Observable<Recipes[]>;
  }

  getAllCountries() {
    return this.http.get(`${environment.url}/countries`) as Observable<Recipes[]>;
  }
}
