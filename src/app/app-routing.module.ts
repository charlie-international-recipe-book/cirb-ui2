import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Country2Component } from './components/country2/country2.component';
import { CreateComponent } from './components/create/create.component';
import { DeleteComponent } from './components/delete/delete.component';
import { EditComponent } from './components/edit/edit.component';
import { FoodComponent } from './components/food/food.component';
import { IngredientComponent } from './components/ingredient/ingredient.component';

const routes: Routes = [
  {
    path: "food",
    component: FoodComponent
  },
  {
    path: "ingredient",
    component: IngredientComponent
  },
  {
    path: "country",
    component: Country2Component
  },
  {
    path: "create",
    component: CreateComponent
  },
  {
    path: "edit",
    component: EditComponent
  },
  {
    path: "delete",
    component: DeleteComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
