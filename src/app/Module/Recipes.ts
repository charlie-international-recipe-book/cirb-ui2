export interface Recipes {

    id?: number;
    food_name?: string;
    location?: string;
    food_price?: any;
    ingredient_name?: any;
    ingredient_details?: any;
    ingredient_price?: any;
    country_name?: any;
    country_likeness?: any;
    Spicy?: boolean;
    Sweet?: boolean;
    Ingredient1?: any;
    Ingredient2?: any;
    Ingredient3?: any;
    Ingredient4?: any;
    Ingredient5?: any;
    
    

}

